#!/usr/bin/env ruby
require 'optparse'
require 'rest-client'
require 'json'
require 'logger'
require 'pp'

class String
  def to_bool
    return true if self == true || self =~ (/(true|t|yes|y|1)$/i)
    return false if self == false || self =~ (/(false|f|no|n|0)$/i)
    raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
  end
end

class StashAPI

  def initialize(http_protocol, stash_host, stash_port, project_key, repo_name, stash_user, stash_password)
    @branch_permissions = RestClient::Resource.new "#{http_protocol}://#{stash_host}:#{stash_port}/rest/branch-permissions/1.0/projects/#{project_key}/repos/#{repo_name}", stash_user, stash_password
    @hook_resource = RestClient::Resource.new "#{http_protocol}://#{stash_host}:#{stash_port}/rest/api/1.0/projects/#{project_key}/repos/#{repo_name}/settings/hooks", stash_user, stash_password
  end

  def restrict_all branch_to_lock
    body = { :type => "BRANCH", :value => "refs/heads/#{branch_to_lock}", :users => ['stash'] }
    @branch_permissions["/restricted"].post body.to_json, :content_type => 'application/json'
  end

  def list_restrictions
    @branch_permissions['/restricted'].get :content_type => 'application/json'
  end

  def remove_restriction_for_id restriction_id
    @branch_permissions["/restricted/#{restriction_id}"].delete :content_type => 'application/json'
  end

  def remove_restriction branch_to_lock
    restrictions = list_restrictions
    restrictions = JSON.parse(restrictions)['values']
    puts restrictions
    restrictions.each do |restriction|
      puts "restriction['value'] = #{restriction['value']}"
      if restriction['value'] =~ /#{branch_to_lock}/
        remove_restriction_for_id restriction['id']
      else
        puts "#{restriction['value']} isn't for branch #{branch_to_lock}"
      end
    end
  end

  def enable_stash_hook hook_key, enable
    if enable
      @hook_resource["/#{hook_key}/enabled"].put '', :content_type => 'application/json'
    else
      @hook_resource["/#{hook_key}/enabled"].delete :content_type => 'application/json'
    end
  end
end


options = {:branch => 'master'}
OptionParser.new do |opts|
    opts.banner = "Usage: stash_api.rb <command> [options]"

      opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
        options[:verbose] = v
      end

      opts.on("-p", "--http-protocol", "--require PROTOCOL", "the http protocol stash is listening on") do |proto|
        options[:http_protocol] = proto
      end

      opts.on("-h", "--stash-host", "--require HOST", "the stash git host IP/DNS") do |host|
        options[:stash_host] = host
      end

      opts.on("-P", "--stash-port", "--require PORT", "the port stash server is listening on for http trafic") do |port|
        options[:stash_port] = port
      end

      opts.on("-k", "--project-key", "--require PROJECT_KEY", "the stash project key for the project you want to interact with") do |key|
        options[:project_key] = key
      end

      opts.on("-n", "--repo-name", "--require REPO_NAME", "the name of the git repository you want to interact with") do |repo|
        options[:repo_name] = repo
      end

      opts.on('-b', "--branch-name", "--optional BRANCH_NAME", "the name of the git repo brach you want to interact with. defaults to master") do |branch|
        options[:branch] = branch
      end

      opts.on("-u", "--stash-user", "--require STASH_USER", "the name of the user you want to use for the stash api (must be an admin for the repo)") do |user|
        options[:stash_user] = user
      end

      opts.on("-c", "--stash-password", "--require STASH_PASSWD", "the password for the user") do |password|
        options[:stash_password] = password
      end

      opts.on("-e", "--command-to-execute", "--require COMMAND", "the command to execute from the script") do |command|
        options[:command] = command
      end
end.parse!

stash = StashAPI.new options[:http_protocol], options[:stash_host], options[:stash_port], options[:project_key],
  options[:repo_name], options[:stash_user], options[:stash_password]

#pp options
#pp ARGV

if options[:command] == "restrict"
  stash.restrict_all options[:branch]
elsif options[:command] == "unrestrict"
  stash.remove_restriction options[:branch]
elsif options[:command] == "hook"
  stash.enable_stash_hook ARGV[0], ARGV[1].to_bool
else
  pp "Command [ #{options[:command]} ] is not a valid command"
end
