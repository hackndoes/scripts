# Print message $2 with log-level $1 to STDERR, colorized if terminal
log() {
        local level=${1?}
        shift
        local code= line="[$(date '+%F %T')] $level: $*"
        if [ -t 2 ]
        then
                case "$level" in
                INFO) code=36 ;;
                DEBUG) code=30 ;;
                WARN) code=33 ;;
                ERROR) code=31 ;;
                *) code=37 ;;
                esac
                echo "[${code}m${line}[0m"
        else
                echo "$line"
        fi >&2
}
 
log "$@"
